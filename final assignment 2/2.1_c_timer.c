#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <signal.h>
#include <sys/mman.h> 
#define MAX_NS (long int) 1e9

sigset_t set;
int wait_a;

void *thread_func(void *arg)
{
	int i = 0;
	struct timespec before;
	struct timespec after;

	timer_t tid;
	struct sigevent sev;
	struct itimerspec ts;
	
	ts.it_interval.tv_sec = 0;
	ts.it_interval.tv_nsec = 1e6;
	ts.it_value.tv_sec = 0;
	ts.it_value.tv_nsec = 1e6;

	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGALRM;
	sev.sigev_value.sival_ptr = &tid;

	pthread_sigmask(SIG_BLOCK, &set, NULL);
	int k = timer_create(CLOCK_MONOTONIC, &sev, &tid);
	if (k == -1)
		printf("Timer Not created\n");	
	else
		printf("Timer Created\n");

	timer_settime(tid, 0, &ts, NULL);
	clock_gettime(CLOCK_REALTIME, &before);

	while (i < 100)
	{
		i++;
		for (int j = 2; j < 1000; j++)
		{
			j = j * j;
		}
				
		sigwait(&set,&wait_a);
	}

	clock_gettime(CLOCK_REALTIME, &after);

	if (before.tv_nsec < after.tv_nsec)
	{
		printf("%ld (s)\n", after.tv_sec - before.tv_sec);
		printf("%f (milis)\n", (after.tv_nsec - before.tv_nsec)/1e6);
	}
	else
	{
		printf("%ld (s)\n", after.tv_sec - before.tv_sec - 1);
		printf("%f (milis)\n", (after.tv_nsec + (MAX_NS - before.tv_nsec))/1e6);
	}

	return 0;

}

int main()
{
	printf("inside_main\n");

	sigemptyset(&set);
	sigaddset(&set, SIGALRM);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	pthread_t newthread;
	pthread_create(&newthread,NULL, thread_func, NULL);
	pthread_join(newthread, NULL);

	printf("done_thread_exceution\n");

}
