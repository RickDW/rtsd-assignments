#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <signal.h>
#include <sys/mman.h> 

#define MAX_NS (long int) 1e9

void *thread_func(void *arg)
{
	int i = 0;
	struct timespec before;
	struct timespec after;
	struct timespec looptime;
	struct timespec abs_time;
	struct timespec deltat;
	
	deltat.tv_sec = 0;
	deltat.tv_nsec = 1000000;
	
	mlockall(MCL_CURRENT | MCL_FUTURE);
	

	clock_gettime(CLOCK_REALTIME, &abs_time);
	clock_gettime(CLOCK_REALTIME, &before);

	while (i < 100)
	{
		i++;
		for (int j = 2; j < 1000; j++)
		{
			j = j * j;
		}

		clock_gettime(CLOCK_REALTIME, &looptime);
		abs_time.tv_sec = looptime.tv_sec + deltat.tv_sec + (looptime.tv_nsec + deltat.tv_nsec) / MAX_NS;
		abs_time.tv_nsec = (looptime.tv_nsec + deltat.tv_nsec) % MAX_NS;
		clock_nanosleep(CLOCK_REALTIME,TIMER_ABSTIME, &abs_time, NULL);
	 
	}

	clock_gettime(CLOCK_REALTIME, &after);

	if (before.tv_nsec < after.tv_nsec)
	{
		printf("%ld (s)\n", after.tv_sec - before.tv_sec);
		printf("%f (milisec)\n", (after.tv_nsec - before.tv_nsec)/1e6);
	}
	else
	{
		printf("%ld (s)\n", after.tv_sec - before.tv_sec - 1);
		printf("%f (milisec)\n", (after.tv_nsec + MAX_NS - before.tv_nsec)/1e6);
	}

	return 0;

}

int main()
{
	printf("inside_main\n");
	
	struct sched_param sch;
	sch.sched_priority = 0;
	
	pthread_attr_t attr;
	pthread_attr_setinheritsched(&attr,PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&attr,SCHED_FIFO);
	pthread_attr_setschedparam(&attr,&sch);

	pthread_t newthread;
	pthread_create(&newthread, NULL, thread_func, NULL);
	pthread_join(newthread, NULL);
	printf("done_thread_exceution\n");

}
