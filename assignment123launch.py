from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    original_webcam = '/image'
    redirected_webcam = '/webcam_input'
    jiwy_webcam_output = '/webcam_output'

    return LaunchDescription([
        # create a webcam stream + reroute it to the jiwy node
        Node(
            package='image_tools',
            executable='cam2image',
            remappings=[
                (original_webcam, redirected_webcam)
            ]
        ),
        # launch jiwy
        Node(
            package='jiwy_simulator',
            executable='jiwy_simulator'
        ),
        # launch the light position detector
        # this takes the jiwy simulator's output feed
        Node(
            package='lightpos',
            executable='lightpos',
            remappings=[
                (original_webcam, jiwy_webcam_output)
            ]
        ),
        # convert the pixel coordinates to the range [-0.8, 0.8] x [-0.6, 0.6]
        Node(
            package='coord_conversion',
            executable='coord_conversion',
            remappings=[
                ('setpoint', 'transformed_lightposition')
            ]
        ),
        # launch the setpoint generator
        Node(
            package='lighttracker',
            executable='lighttracker',
            remappings=[
                ('lightposition', 'transformed_lightposition')
            ]
        ),
        # display jiwy's webcam output stream
        Node(
            package='image_tools',
            executable='showimage',
            remappings=[
                (original_webcam, jiwy_webcam_output)
            ]
        )
    ])