#include <cstdio>
#include <chrono>
#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "rtsd_interfaces/msg/point2.hpp"

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

class SetpointGenerator : public rclcpp::Node
{
public:
  SetpointGenerator()
  : Node("setpoint_generator"), state(false)
  {
    publisher_ = this->create_publisher<rtsd_interfaces::msg::Point2>("setpoint", 10);
    timer_ = this->create_wall_timer(
      5000ms, std::bind(&SetpointGenerator::timer_callback, this));
  }

private:
  void timer_callback()
  {
    auto message = rtsd_interfaces::msg::Point2();

    if (state) {
      message.x = 0.1;
      message.y = 0.1;
    }
    else {
      message.x = -0.1;
      message.y = -0.1;
    }

    state = !state;
    
    RCLCPP_INFO(this->get_logger(), "Publishing: '%f, %f'", message.x, message.y);
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<rtsd_interfaces::msg::Point2>::SharedPtr publisher_;
  bool state;
};


int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<SetpointGenerator>());
  rclcpp::shutdown();
  return 0;
}
