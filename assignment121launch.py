from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    original_webcam_topic = '/image'
    jiwy_webcam_input = '/webcam_input'
    jiwy_webcam_output = '/webcam_output'

    return LaunchDescription([
        # create a webcam stream + reroute it to the jiwy node
        Node(
            package='image_tools',
            executable='cam2image',
            remappings=[
                (original_webcam_topic, jiwy_webcam_input)
            ]
        ),
        # launch jiwy
        Node(
            package='jiwy_simulator',
            executable='jiwy_simulator'
        ),
        # launch the setpoint generator
        Node(
            package='setpoint_generator',
            executable='setpoint_generator'
        ),
        # display jiwy's webcam output stream
        Node(
            package='image_tools',
            executable='showimage',
            remappings=[
                ('/image', jiwy_webcam_output)
            ]
        )
    ])