This directory contains ROS2 packages. How these packages relate to the
subassignments is explained below:

1.1.2: lightdetector
1.1.3: lightdetector
1.1.4: lightpos

1.2.1: setpoint_generator + the assignment121launch.py launch file
1.2.2: lightpos, coord_conversion + the assignment122launch.py launch file