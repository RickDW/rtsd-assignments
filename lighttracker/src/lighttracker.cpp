#include <cstdio>
#include <chrono>
#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "rtsd_interfaces/msg/point2.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

class LightTracker : public rclcpp::Node
{
public:
  LightTracker()
  : Node("lighttracker")
  {
    publisher_ = this->create_publisher<rtsd_interfaces::msg::Point2>("setpoint", 10); // the setpoint topic is used by jiwy
    subscription_ = this->create_subscription<rtsd_interfaces::msg::Point2>("/lightposition", 10, 
        std::bind(&LightTracker::lightposition_callback, this, _1)); // the position of the detected light
    timer_ = this->create_wall_timer(
      500ms, std::bind(&LightTracker::timer_callback, this)); // warning: if the timer's period is changed, also change dt
  }

private:
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<rtsd_interfaces::msg::Point2>::SharedPtr publisher_;
  rclcpp::Subscription<rtsd_interfaces::msg::Point2>::SharedPtr subscription_;

  // the position of the detected light
  float xpos = 0.0;
  float ypos = 0.0;

  // where should the detected light be to be centered in the camera feed?
  float neutral_xpos = 0.0;
  float neutral_ypos = 0.0;

  float tau = 1.0; // seconds, time constant of the controller
  float dt = 0.5; // seconds, time in between setpoint updates (warning: should equal the period of the timer)

  void lightposition_callback(const rtsd_interfaces::msg::Point2::ConstSharedPtr message)
  {
    // update the position of the detected light
    xpos = message->x;
    ypos = message->y;
  }

  void timer_callback()
  {
    // determine the new setpoint for the jiwy arm

    auto setpoint = rtsd_interfaces::msg::Point2();

    // determine the desired velocity of the jiwy arm
    float xvel = (xpos - neutral_xpos) / tau;
    float yvel = (ypos - neutral_ypos) / tau;

    // determine the new setpoint (based on forward Euler integration)
    setpoint.x = xpos + dt * xvel;
    setpoint.y = ypos + dt * yvel;
    
    // RCLCPP_INFO(this->get_logger(), "Publishing: '%f, %f'", message.x, message.y);
    // send the new setoint to jiwy
    publisher_->publish(setpoint);
  }
};


int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<LightTracker>());
  rclcpp::shutdown();
  return 0;
}

