from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    original_webcam = '/image'
    redirected_webcam = '/webcam_input'
    jiwy_webcam_output = '/webcam_output'

    return LaunchDescription([
        # create a webcam stream + reroute it to the jiwy node
        Node(
            package='image_tools',
            executable='cam2image',
            remappings=[
                (original_webcam, redirected_webcam)
            ]
        ),
        # launch the light position detector
        Node(
            package='lightpos',
            executable='lightpos',
            remappings=[
                (original_webcam, redirected_webcam)
            ]
        ),
        # convert the pixel coordinates to the range [-0.8, 0.8] x [-0.6, 0.6]
        Node(
            package='coord_conversion',
            executable='coord_conversion'
        ),
        # launch jiwy
        Node(
            package='jiwy_simulator',
            executable='jiwy_simulator'
        ),
        # display jiwy's webcam output stream
        Node(
            package='image_tools',
            executable='showimage',
            remappings=[
                (original_webcam, jiwy_webcam_output)
            ]
        )
    ])