#include <functional>
#include <memory>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"
#include "sensor_msgs/msg/image.hpp"

using std::placeholders::_1;
using namespace std::chrono_literals;
 

class LightDetector : public rclcpp::Node {
	public:
		LightDetector() : Node("LightDetector") {
			subscription_ = this->create_subscription<sensor_msgs::msg::Image>("/image", 
				10, std::bind(&LightDetector::topic_callback, this, _1));

			publisher_ = this->create_publisher<std_msgs::msg::Bool>("/lightdetector", 10);

			this->declare_parameter("threshold", threshold); 
			timer_ = this->create_wall_timer( 500ms, std::bind(&LightDetector::respond, this));
		}
		
		void respond() {
			this->get_parameter("threshold", threshold);	
		}
		
		
private:
	int threshold = 100;
	bool lighton = false;

	rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr subscription_;
	rclcpp::TimerBase::SharedPtr timer_;
	rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr publisher_;

	void topic_callback(const sensor_msgs::msg::Image::ConstSharedPtr message) {
		unsigned int sum=0;
		int avg = 0;
		auto step = message->step;
		auto rows = message->height;

		for (unsigned int i =0; i<step*rows; i++) {
			sum = sum + message->data[i]; 
		}
		avg = (int) (sum/(step*rows));

		bool lighton_now = (avg > threshold);

		if ((!lighton && lighton_now) || (lighton && !lighton_now)) {
			// the light has either switched on or off since the last frame
			auto msg = std_msgs::msg::Bool();

			if (!lighton && lighton_now) {
				msg.data = true; // encodes the fact that the light has turned on
			}
			else if (lighton && !lighton_now) {
				msg.data = false; // encodes the fact that the light has turned off
			}

			// std::string description = msg.data ? "The light has turned on" : "The light has turned off";

			// RCLCPP_INFO(this->get_logger(), "%s, Average: '%d'", description.c_str(), avg);
			// RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", description.c_str());

			publisher_->publish(msg);

			lighton = lighton_now;
		}
	}
};


int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<LightDetector>());
  rclcpp::shutdown();
  return 0;
}











