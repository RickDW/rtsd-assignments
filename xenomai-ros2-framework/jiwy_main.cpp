#include <stdio.h>
#include <unistd.h>
#include <iterator>
#include <signal.h>
#include <vector>
#include <sys/syscall.h>

#include "framework/multiCommClass.h"
#include "framework/runnableClass.h"
#include "framework/superThread.h"
#include "framework/icoCommClass.h"

// #include <Your 20-sim-code-generated h-file?> Don't forget to compile the cpp file by adding it to CMakeLists.txt
#include "20sim/ControllerTilt.h"
#include "20sim/ControllerPan.h"
#include "20sim/Plant.h"


volatile bool exitbool = false;

void exit_handler(int s)
{
    printf("Caught signal %d\n", s);
    exitbool = true;
}

int main()
{
    //CREATE CNTRL-C HANDLER
    signal(SIGINT, exit_handler);

    printf("Press Ctrl-C to stop program\n"); // Note: this will 
        // not kill the program; just jump out of the wait loop. Hence,
        // you can still do proper clean-up. You are free to alter the
        // way of determining when to stop (e.g., run for a fixed time).


    // CONFIGURE, CREATE AND START THREADS HERE

    // Pancontroller :: inputs & outputs

    int iddp_inputparams_pancont[] = {0};
    frameworkComm *pancont_inputports[] = {
        new IDDPComm(2, -1, 1, iddp_inputparams_pancont)
    };
    int pancont_inputports_length = 1;

    int iddp_outputparams_pancont[] = {0};
    frameworkComm *pancont_outputports[] = {
        new IDDPComm(1, -1, 1, iddp_outputparams_pancont)
    };
    int pancont_outputports_length = 1;

    PanController *pancont = new PanController;
    runnable *runnable_pancont = new wrapper20<PanController>(
        pancont, pancont_inputports, pancont_outputports,
        pancont_inputports_length, pancont_outputports_length
    );

    // Tiltcontroller :: inputs & outputs

    int iddp_inputparams_tiltcont[] = {0};
    frameworkComm *tiltcont_inputports[] = {
        new IDDPComm(3, -1, 1, iddp_inputparams_tiltcont)
    };
    int tiltcont_inputports_length = 1;

    int iddp_outputparams_tiltcont[] = {1};
    frameworkComm *tiltcont_outputports[] = {
        new IDDPComm(4, -1, 1, iddp_outputparams_tiltcont)
    };
    int tiltcont_outputports_length = 1;

    TiltController *tiltcont = new TiltController;
    runnable *runnable_tiltcont = new wrapper20<TiltController>(
        tiltcont, tiltcont_inputports, tiltcont_outputports,
        tiltcont_inputports_length, tiltcont_outputports_length
    );

    // Plant :: inputs & outputs

    int iddp_inputparams_plant_pan[] = {0};
    int iddp_inputparams_plant_tilt[] = {1};
    frameworkComm *plant_inputports[] = {
        new IDDPComm(1, -1, 1, iddp_inputparams_plant_pan),
        new IDDPComm(4, -1, 1, iddp_inputparams_plant_tilt)
    };
    int plant_inputports_length = 2;

    int iddp_outputparams_plant_pan[] = {0};
    int iddp_outputparams_plant_tilt[] = {1};
    frameworkComm *plant_outputports[] = {
        new IDDPComm(2, -1, 1, iddp_outputparams_plant_pan)
        new IDDPComm(3, -1, 1, iddp_outputparams_plant_tilt)
    };
    int plant_outputports_length = 2;

    Plant *plant = new Plant;
    runnable *runnable_plant = new wrapper20<Plant>(
        plant, plant_inputports, plant_outputports,
        plant_inputports_length, plant_outputports_length
    );

    // OLD CODE BELOW, IGNORE
    
    // Create param and wrapper for controller
    // int iddpcontroller_uParam[] = {0, 1};
    // int xddptiltcontroller_uParam[] = {2};
    // int xddppancontroller_uParam[] = {3};

    // frameworkComm *controller_uPorts[] = {
    //     new IDDPComm(5, -1, 2, iddpcontroller_uParam),
    //     new XDDPComm(10, -1, 1, xddptiltcontroller_uParam),
    //     new XDDPComm(11, -1, 1, xddppancontroller_uParam)
    // };

    // int controller_yParam[] = {0, 1};
    // int controller_yParam_logging[] = {0};

    // frameworkComm *controller_yPorts[] = {
    //     new IDDPComm(2, 3, 2, controller_yParam)
    // };

    // Controller *controller_class = new Controller;
    // runnable *controller_runnable = new wrapper<Controller>(
    //     controller_class, controller_uPorts, controller_yPorts, 3, 1
    // );
    // controller_runnable->setSize(4, 2);

    // // create param and wrapper for plant
    // int plant_uParam[] = {0, 1};
    // frameworkComm *plant_uPorts[] = {
    //     new IDDPComm(3, -1, 2, plant_uParam)
    // };

    // int plant_yParam[] = {0, 1};
    // int plant_yParam_logging[] = {0};
    // frameworkComm *plant_yPorts[] = {
    //     new IDDPComm(4, 5, 2, plant_yParam)
    // };

    // Plant *plant_class = new Plant;
    // runnable *plant_runnable = new wrapper<Plant>(
    //     plant_class, plant_uPorts, plant_yPorts, 1, 1
    // );
    // plant_runnable->setSize(2, 2);

    // // init xenothread for controller + plant
    // xenoThread plantClass(plant_runnable);
    // xenoThread controllerClass(controller_runnable);
    // plantClass.init(1000000, 99, 1);
    // controllerClass.init(1000000, 98, 0);

    // plantClass.enableLogging(true, 25);
    // controllerClass.enableLogging(true, 26);

    // // start threads
    // controllerClass.start("controller");
    // plantClass.start("plant");

    // WAIT FOR CNTRL-C
    timespec t = {.tv_sec=0, .tv_nsec=100000000}; // 1/10 second

    while (!exitbool)
    {
        // Let the threads do the real work
        nanosleep(&t, NULL);
        // Wait for Ctrl-C to exit
    }
    printf("Ctrl-C was pressed: Stopping gracefully...\n");

    //CLEANUP HERE
    plant.stopThread();
    pancont.stopThread();
    tiltcont.stopThread();

    runnable_plant->~xenoThread();
    runnable_pancont->~xenoThread();
    runnable_tiltcont->~xenoThread();

    runnable_plant->~runnable();
    runnable_pancont->~runnable();
    runnable_tiltcont->~runnable();

    return 0;
}