#include <stdlib.h>
#include <functional>
#include "rclcpp/rclcpp.hpp"
#include <cv_bridge/cv_bridge.h>
#include "sensor_msgs/image_encodings.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "rtsd_interfaces/msg/point2.hpp"


using std::placeholders::_1;


static const std::string WINDOW1 = "Color Window";
static const std::string WINDOW2 = "Grey Window";
static const std::string WINDOW3 = "Spot Window";


class lightpos : public rclcpp::Node {

public:
    
    lightpos() : Node("lightposindicator") {
        // cv::namedWindow(WINDOW1);
        // cv::namedWindow(WINDOW2);
        // cv::namedWindow(WINDOW3);
    
        subscription_ = this->create_subscription<sensor_msgs::msg::Image>("/image", 10, 
            std::bind(&lightpos::opencvfunction, this, _1));

        publisher_ = this->create_publisher<rtsd_interfaces::msg::Point2>("/lightposition", 10);
    };

private:
    rclcpp::Publisher<rtsd_interfaces::msg::Point2>::SharedPtr publisher_;
    rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr subscription_;

   void opencvfunction(const sensor_msgs::msg::Image::ConstSharedPtr msg) {
        
        cv_bridge::CvImagePtr cv_ptr;
        cv_ptr = cv_bridge::toCvCopy(msg);
        
        // cv::imshow(WINDOW1, cv_ptr->image);
        // cv::waitKey (10);

     //   cv::destroyWindow("Color Window");
        
       
        cv::Mat colorimg = cv_ptr->image;
        cv::Mat greyimg;
        cv::Mat thresholdimg;
        
        cv::cvtColor(colorimg,greyimg,cv::COLOR_RGB2GRAY);
        // cv::imshow(WINDOW2,greyimg);
        // cv::waitKey (10);

    //    cv::destroyWindow("Grey Window");
        
       
        cv::threshold(greyimg,thresholdimg,125,255,cv::THRESH_BINARY+cv::THRESH_OTSU);
        // cv::imshow(WINDOW3,thresholdimg); 
        // cv::waitKey (10);

        // cv::destroyWindow("Spot Window");
        cv::Moments m = cv::moments(thresholdimg,true);
        cv::Point p(m.m10/m.m00, m.m01/m.m00);
        std::cout<< cv::Mat(p)<< std:: endl;
        

        //create a message and publish it to the /lightposition topic
        auto message = rtsd_interfaces::msg::Point2();
        message.x = p.x;
        message.y = p.y;

        publisher_->publish(message);
    }
};


int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<lightpos>());
    rclcpp::shutdown();
    return 0;
} 
