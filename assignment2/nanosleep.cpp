#include <pthread.h>
// #include <unistd.h>
#include <time.h>
#include <stdio.h>

void* thread_func(void * arg) {

    int i = 0;

    // 1e6 ns = 1 ms
	struct timespec time_interval;
	time_interval.tv_sec = 0;
  	time_interval.tv_nsec = 1000000;

    struct timespec before;
    struct timespec after;

    int square = 0;

  	clock_gettime(CLOCK_REALTIME, &before);

    while (i < 1e3)
    {
        i++;

        // do some work
        square = i*i;

        // no IO inside the thread when measuring performance!!
        // printf("inside_thread\n");

        nanosleep(&time_interval, NULL);
    }

  	clock_gettime(CLOCK_REALTIME, &after);

    printf("Time elapsed with %d iterations:\n", i);

    if (before.tv_nsec < after.tv_nsec) {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec); 
        printf("%ld (ns)\n", after.tv_nsec - before.tv_nsec);
    }
    else {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec - 1); 
        printf("%ld (ns)\n", after.tv_nsec + ((long int)1e9 - before.tv_nsec));
    }

    return 0;
      
}


int main() {

    printf("inside_main\n");
    pthread_t newthread;
    pthread_create(&newthread, NULL, thread_func, NULL);
    pthread_join(newthread,NULL);
    printf("done_thread_exceution\n");
}
