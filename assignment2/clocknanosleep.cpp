#include <pthread.h>
// #include <unistd.h>
#include <time.h>
#include <stdio.h>


#define MAX_NS (long int) 1e9


void* thread_func(void * arg) {

    int i = 0;

    // 1e6 ns = 1 ms
	struct timespec deltat;
	deltat.tv_sec = 0;
  	deltat.tv_nsec = 1000000;

    struct timespec current_time;
    struct timespec abs_time_interval;

    struct timespec before;
    struct timespec after;

    int square = 0;

  	clock_gettime(CLOCK_REALTIME, &before);

    while (i < 1e3)
    {
        i++;

        // do some work
        square = i*i;

        // no IO inside the thread when measuring performance!!
        // printf("inside_thread\n");

        // determine the absolute time at which the thread should resume
        clock_gettime(CLOCK_REALTIME, &current_time);
        abs_time_interval.tv_sec = current_time.tv_sec + deltat.tv_sec + (current_time.tv_nsec + deltat.tv_nsec) / MAX_NS;
        abs_time_interval.tv_nsec = (current_time.tv_nsec + deltat.tv_nsec) % MAX_NS;

        clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &abs_time_interval, NULL);
    }

  	clock_gettime(CLOCK_REALTIME, &after);

    printf("Time elapsed with %d iterations:\n", i);

    if (before.tv_nsec < after.tv_nsec) {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec); 
        printf("%ld (ns)\n", after.tv_nsec - before.tv_nsec);
    }
    else {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec - 1); 
        printf("%ld (ns)\n", after.tv_nsec + (MAX_NS - before.tv_nsec));
    }

    return 0;
      
}


int main() {

    printf("inside_main\n");
    pthread_t newthread;
    pthread_create(&newthread, NULL, thread_func, NULL);
    pthread_join(newthread,NULL);
    printf("done_thread_exceution\n");
}
