#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <signal.h>

#define MAX_NS (long int) 1e9   

sigset_t set;
int wait_a;


void* thread_func(void * arg) {

	int i = 0;
	struct timespec deltat;
	struct timespec before;
    	struct timespec after;
    	struct timespec abs_time;
	deltat.tv_sec = 0;
  	deltat.tv_nsec = 1000000;
  	
  	
	clock_gettime(CLOCK_REALTIME, &abs_time);
  		
 	
  	
  	abs_time.tv_sec = before.tv_sec + deltat.tv_sec + (before.tv_nsec + deltat.tv_nsec) / MAX_NS;
        abs_time.tv_nsec = (before.tv_nsec + deltat.tv_nsec) % MAX_NS;
        
        
	
	
        
    	timer_t tid;
    	struct sigevent   sev;	
    	struct itimerspec ts;
    	ts.it_interval.tv_sec = 0;
    	ts.it_interval.tv_nsec = 1e6;
    	//ts.it_value = abs_time;
    	ts.it_value.tv_sec = 0;
    	ts.it_value.tv_nsec = 1000000;
    	
   	 sev.sigev_notify = SIGEV_SIGNAL;
    	sev.sigev_signo = SIGALRM;
    	sev.sigev_value.sival_ptr = &tid;
    
    
    	int rc = pthread_sigmask(SIG_BLOCK, &set, NULL); 
         
    	int k = timer_create(CLOCK_REALTIME,&sev, &tid);
    		if(k==-1)
        	printf("Timer Not created\n");
    		else
        	printf("Timer Created\n");
         
    	timer_settime(tid, 0 , &ts, NULL);
    	
    	clock_gettime(CLOCK_REALTIME, &before);
 	
 
 	
 	

   	while (i<100) 
    	{
    		i++;
        	for(int j=2; j<1000;j++)
        	 {
        	 	j= j*j;
        	 }
            	//nanosleep(&deltat, NULL);
           //  clock_nanosleep(CLOCK_REALTIME, 0, &abs_time, NULL);
            	
             sigwait(&set,&wait_a);
    	}
    
    
	clock_gettime(CLOCK_REALTIME, &after);
       
        
        
        if (before.tv_nsec < after.tv_nsec) {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec); 
        printf("%ld (ns)\n", (after.tv_nsec - before.tv_nsec));
    	}
    else {
        printf("%ld (s)\n", after.tv_sec - before.tv_sec - 1); 
        printf("%ld (ns)\n", (after.tv_nsec + (MAX_NS - before.tv_nsec)));
    	}
        
        

        return 0;
       
             
}


int main() {

    printf("inside_main\n");
    pthread_t newthread;   
    sigemptyset(&set);
    sigaddset(&set, SIGALRM);
    sigprocmask(SIG_BLOCK, &set, NULL);
         
    pthread_create(&newthread, NULL, thread_func, NULL);
    pthread_join(newthread,NULL);
    printf("done_thread_exceution\n");
    
    
}
