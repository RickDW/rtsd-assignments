#include <stdlib.h>
#include <functional>
#include "rclcpp/rclcpp.hpp"
#include "rtsd_interfaces/msg/point2.hpp"


using std::placeholders::_1;


class CoordConversion : public rclcpp::Node {

public:    
    CoordConversion() : Node("CoordConversion") {
    
        subscription_ = this->create_subscription<rtsd_interfaces::msg::Point2>("/lightposition", 10, 
            std::bind(&CoordConversion::conversion_callback, this, _1));

        publisher_ = this->create_publisher<rtsd_interfaces::msg::Point2>("/setpoint", 10);
    };

private:
    rclcpp::Publisher<rtsd_interfaces::msg::Point2>::SharedPtr publisher_;
    rclcpp::Subscription<rtsd_interfaces::msg::Point2>::SharedPtr subscription_;

    float horizontal_range = 0.8;
    float vertical_range = 0.6;

   void conversion_callback(const rtsd_interfaces::msg::Point2::ConstSharedPtr pixelcoords) {
        //create a message and publish it to the /lightposition topic
        auto message = rtsd_interfaces::msg::Point2();
        float x_normalized = pixelcoords->x / 300 * 2 - 1; // left: -1, right: 1
        float y_normalized = pixelcoords->y / 230 * 2 - 1; // top: -1, bottom: 1

        message.x = x_normalized * horizontal_range; // rescale
        message.y = y_normalized * vertical_range * -1; // rescale and flip

        publisher_->publish(message);
    }
};


int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<CoordConversion>());
    rclcpp::shutdown();
    return 0;
} 